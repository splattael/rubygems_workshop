# RubyGems workshop

In this workshop, partiticipants will collaborate with each other to:

* create a new gem,
* understand the directories involved,
* write some simple new feature,
* and write some tests

For advanced participants, they could elect to 

* extract functionality out from [monorepo into gems](https://gitlab.com/groups/gitlab-org/-/epics/10869).

## Prerequisites

You will require a computer with either:

* Linux operating system
* MacOS operating system

Windows operating system is acceptable but the workshop coaches
may not be as familar.
It may be helpful for this workshop to install WSL.

## Installing Ruby

It is recommended that you have at least Ruby 3.1 installed.
Ask the workshop coaches for help on how to install Ruby.

## Setup Git

At GitLab, we use Git for version control.

If you have Git installed, please setup your Git email and username:

```sh
git config --global user.name "<YOUR NAME>"
git config --global user.email "<YOUR_EMAIL>"
```

## Creating a new gem

Now you have Ruby installed, ensure you also have `bundler` installed:

```sh
gem install bundler
```

Choose a new name for your gem. For this workshp, we will use the name `XXX`.
To create a new gem, type:

```sh
bundle gem XXX
```

You may need to answer with `y`, or `n` to a few options.

If this command was succesful, you will see that a new directory `XXX` was created
for you, with some files inside the directory.

## Understand the directories and files inside a gem

## Add a simple new feature

## Write a test for the new feature

## Push to GitLab

## Credits

This is based on the <https://bundler.io/guides/creating_gem.html>.